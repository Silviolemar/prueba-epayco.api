# Epayco Wallet

## Install

- Clone this repository: `git clone https://gitlab.com/Silviolemar/prueba-epayco.api.git`

## Prerequisites

You need git to clone the repository. You can get git from <http://git-scm.com/>.

A number of node.js tools is necessary to initialize and test the project. You must have node.js and its package manager (npm) installed. You can get them from <http://nodejs.org/>. The tools/modules used in this project are listed in package.json and include express, mongodb and mongoose.

## Start the MongoDB server

First we need to create the db directory where the database files will live in. In your terminal navigate to the root of your system by doing cd .. until you reach the top directory. You can create the directory by running sudo mkdir -p /data/db. Now open a different tab in your terminal and run mongod to start the Mongo server.

## Run the Application

The project is preconfigured with a simple development web server. The simplest way to start this server is:

npm run dev

## Generate API documentation

npm run docs

The command will generate a /doc folder that will contain an index.html file. Open it in any browser. The file contains information about API routes. (
not implemented)

## Project commands

- Build Setup

  ```bash
  # Install dependencies
  $ npm install

  # Configure the .env file from .env.example file.

  # Serve with hot reload at localhost:5000 to dev
  $ npm run dev

  # Serve with hot reload at localhost:5000 to production
  $ npm run start

  # Generate API documentation
  $ npm run docs

  # See API documentation
  http://localhost:5000/apidoc

  # Confirms that the server is working
  http://localhost:5000/

## Contribute

¡All contributions are welcome! Please read the guidelines [contribución](https://gitlab.com/corporativo.matacta/servicio-express-servicios/blob/master/CONTRIBUTING.md) first.

## Project Structure

\- api
^- Controladores, modelos, rutas, scripts
Ejemplo: limpiar base de datos, crear base de datos,
crar administrador web, etc.
\- config
^- Archivos con configuración
Ejemplo: base de datos, API externar, etc.
\- docs
^- Información y documentación del proyecto
\- public
^- Esto es lo que servirá el servidor
Pero puede incluir otros archivos
\- .gitab
^- Plantillas de gitlab

## More documentation about Express.js

Documentación oficial [Express.js docs](https://expressjs.com).
