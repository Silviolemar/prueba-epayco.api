/**
 * @api {post} /wallets/charge Recargar billetera
 * @apiName charge
 * @apiVersion 0.0.0
 * @apiGroup Wallet
 *
 * @apiErrorExample {json} Error-Response:
 *  HTTP/1.1 500 Internal Server Error
 *  {
 *      {
 *          "status": "ERROR",
 *          "message": "Cannot read property 'userId' of undefined"
 *      }
 *  }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          {
 *              "status": "ERROR",
 *              "message": "Incorrect data"
 *          }
 *     }
 * @apiSuccessExample {Object} Success
 *     HTTP/1.1 200 OK
 *     {
 *         {
 *             "status": "OK",
 *             "message": "Recharge successful"
 *         }
 *     }
 *
 */
