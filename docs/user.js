/* eslint-disable max-len */
/**
 * @api {post} /users/create Registro de usuario
 * @apiName create
 * @apiVersion 0.0.0
 * @apiGroup User
 *
 * @apiParam {String} name Nombre del usuario.
 * @apiParam {String} email Correo electronico.
 * @apiParam {String} password Contraseña.
 * @apiParam {String} document Documento de identidad.
 * @apiParam {String} phone Celular.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          {
 *              "status": "INCORRECT_EMAIL",
 *              "message": "Incorrect email"
 *          }
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          {
 *              "status": "DUPLICATED_VALUES",
 *              "message": "Duplicated user"
 *          }
 *     }
 * @apiSuccessExample {Object} Success
 *     HTTP/1.1 200 OK
 *     {
 *         {
 *             "status": "OK",
 *             "message": "user created"
 *         }
 *     }
 *
 */

/**
 *
 * @api {post} /users/login Login
 * @apiName login
 * @apiVersion 0.0.0
 * @apiGroup User
 *
 * @apiParam {String} email Email de acceso.
 * @apiParam {String} password Contraseña de acceso.
 *
 * @apiError BadRequest Ingreso de datos erróneos.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *          {
 *              "status": "INVALID_PASSWORD",
 *              "message": ""
 *          }
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *          {
 *              "status": "USER_NOT_FOUND",
 *              "message": ""
 *          }
 *     }
 *
 * @apiSuccess {Boolean} ok Estado de la petición.
 * @apiSuccess {Object} data  Objeto Usuario.
 * @apiSuccess {String} token  Token del Usuario.
 * @apiSuccessExample {Object} Success
 * {
 *      {
 *          "status": "OK",
 *          "data": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZjAyMjZkODM4MjQzYzMwZTJiN2I2NWMiLCJpYXQiOjE1OTM5ODEyNjcsImV4cCI6MTU5Mzk4MTg2N30.ML_SnPIo3IoyukxPQ58bTLgugIG0zZoD26wEVxxbERo",
 *              "email": "silviolemarr@gmail.com",
 *              "userName": "Silvio",
 *              "wallet": {
 *                  "_id": "5f0226d838243c30e2b7b65d",
 *                  "name": "Billetera Silvio",
 *                  "value": 100,
 *                  "__v": 0
 *              },
 *              "expiresIn": 600
 *          }
 *      }
 * }
 */
