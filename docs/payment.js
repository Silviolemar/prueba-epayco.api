/**
 * @api {post} /payments/sendMail Enviar correo electrónico
 * @apiName sendMail
 * @apiVersion 0.0.0
 * @apiGroup Payment
 *
 * @apiParam {String} token Token de la sesión del usuario.
 * @apiParam {String} userEmail Correo electrónico.
 * @apiParam {String} orderNumber Número de la orden de compra.
 * @apiParam {Number} amount Monto de la compra.
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 422 Bad Request
 *     {
 *          {
 *              "status": "ERROR",
 *              "message": "Incorrect data"
 *          }
 *     }
 * @apiSuccessExample {Object} Success
 *     HTTP/1.1 200 OK
 *     {
 *          {
 *              "status": "OK",
 *              "message": "Mail successfully sent",
 *              "data": {
 *              "randToken": "295a99",
 *              "orderNumber": "1234",
 *              "amount": 999.99
 *          }
 *     }
 *
 */

/**
 * @api {post} /payments/savePayment Guardar pago y descontar billetera
 * @apiName savePayment
 * @apiVersion 0.0.0
 * @apiGroup Payment
 *
 * @apiParam {String} token Token de la sesión del usuario.
 * @apiParam {String} orderNumber Número de la orden de compra.
 * @apiParam {String} amount Monto de la compra.
 * @apiParam {String} walletId Id de la wallet a descontar.
 * @apiErrorExample {json} Error-Response:
 *  HTTP/1.1 500 Internal Server Error
 *  {
 *      {
 *          "status": "ERROR",
 *          "message": "Cannot read property 'userId' of undefined"
 *      }
 *  }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          {
 *              "status": "ERROR",
 *              "message": "Incorrect data"
 *          }
 *     }
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *          {
 *              "status": "DUPLICATED_VALUES",
 *              "message": "Duplicated values"
 *          }
 *     }
 * @apiSuccessExample {Object} Success
 *     HTTP/1.1 200 OK
 *     {
 *          {
 *              "status": "OK",
 *              "message": "Mail successfully sent",
 *              "data": {
 *              "randToken": "295a99",
 *              "orderNumber": "1234",
 *              "amount": 999.99
 *          }
 *     }
 *
 */
