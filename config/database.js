const mongoose = require('mongoose');
const config = require('./config');

const {
  db: {
    credentials, host, port, name,
  },
} = config;

const connectionString = `mongodb://${credentials}${host}:${port}/${name}`;
console.log('Conectando: ', connectionString);

mongoose
  .connect(connectionString, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB connected...'))
  .catch((error) => {
    console.error('Error Database Connection.');
    console.error(error);
    process.exit(500);
  });
