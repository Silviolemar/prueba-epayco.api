/* eslint-disable radix */
const dotenv = require('dotenv');

dotenv.config();

const env = process.env.NODE_ENV; // 'dev' or 'test'

/*
=================================
Base de datos
=================================
*/

const credentials = {
  test: {
    user: encodeURIComponent(process.env.TEST_DB_USER) || '',
    pwd: encodeURIComponent(process.env.TEST_DB_PASSWORD) || '',
  },
  dev: {
    user: encodeURIComponent(process.env.DEV_DB_USER) || '',
    pwd: encodeURIComponent(process.env.DEV_DB_PASSWORD) || '',
  },
};

let dbAuthString = '';

const { user, pwd } = credentials[env];

if (user !== '' && typeof user === 'string') {
  dbAuthString = `${user}:${pwd}@`;
}

const dev = {
  app: {
    port: parseInt(process.env.DEV_APP_PORT) || 9000,
  },
  db: {
    credentials: dbAuthString,
    host: process.env.DEV_DB_HOST || 'localhost',
    port: parseInt(process.env.DEV_DB_PORT) || 27017,
    name: process.env.DEV_DB_NAME || 'dev',
  },
};

const test = {
  app: {
    port: parseInt(process.env.TEST_APP_PORT) || 9000,
  },
  db: {
    credentials: dbAuthString,
    host: process.env.TEST_DB_HOST || 'localhost',
    port: parseInt(process.env.TEST_DB_PORT) || 27017,
    name: process.env.TEST_DB_NAME || 'test',
  },
};

const config = {
  dev,
  test,
};

/*
=================================
Vencimiento del token
=================================
*/

process.env.CADUCIDAD_TOKEN = '48h';

/*
=================================
Seed de autenticacion
=================================
*/

process.env.SEED_AUTENTICACION = process.env.SEED_AUTENTICACION || 'este-es-el-seed-desarrollo';

module.exports = config[env];
