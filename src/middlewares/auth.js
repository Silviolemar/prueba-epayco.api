/* eslint-disable prefer-destructuring */
const jwt = require('jsonwebtoken');

const isAuth = (req, res, next) => {
  if (req.headers.authorization === undefined) {
    res.status(400).send({ status: 'TOKEN_REQUIRED', msg: 'Token required' });
    return;
  }

  try {
    let token;
    if (req.headers.authorization.split(' ')[1]) {
      token = req.headers.authorization.split(' ')[1];
    } else {
      token = req.headers.authorization;
    }
    if (token) {
      const data = jwt.verify(token, process.env.JWT_SECRET);
      req.sessionData = { userId: data.userId };
      next();
    } else {
      res.status(403).send({ ok: false, msg: 'ACCESS_DENIED' });
    }
  } catch (e) {
    res
      .status(e.code || 500)
      .send({ status: e.status || 'ERROR', message: e.message });
  }
};

module.exports = { isAuth };
