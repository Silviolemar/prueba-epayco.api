const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const ProgressBar = require('progress');
const cors = require('cors');
const routesV1 = require('./routes/v1');
require('../config/database');

const app = express();
dotenv.config();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
routesV1(app);

const port = process.env.API_PORT || 9000;
const hostname = process.env.HOST_NAME || '127.0.0.1';

app.get('/', (req, res) => {
  res.send('API!');
});

app.use(logger('dev'));
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.listen(port, () => {
  const bar = new ProgressBar(':bar', { total: 10 });
  const timer = setInterval(() => {
    bar.tick();
    if (bar.complete) {
      clearInterval(timer);
    }
  }, 100);
  console.log(`Server running at http://${hostname}:${port}/`);
});
