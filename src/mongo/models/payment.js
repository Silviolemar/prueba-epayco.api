const mongoose = require('mongoose');

const { Schema } = mongoose;

const paymentSchema = new Schema({
  orderNumber: { type: String, require: true },
  amount: { type: Number, required: true },
  wallet: { type: mongoose.Schema.Types.ObjectId, ref: 'Wallet', required: true },
});

const model = mongoose.model('Payment', paymentSchema);

module.exports = model;
