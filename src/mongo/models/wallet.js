const mongoose = require('mongoose');

const { Schema } = mongoose;

const walletSchema = new Schema({
  name: { type: String },
  value: { type: Number, required: true },
});

const model = mongoose.model('Wallet', walletSchema);

module.exports = model;
