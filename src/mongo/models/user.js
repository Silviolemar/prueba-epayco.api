const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  document: { type: String, required: true },
  name: { type: String, required: true },
  phone: { type: String, required: true },
  wallet: { type: mongoose.Schema.Types.ObjectId, ref: 'Wallet', required: true },
});

const model = mongoose.model('User', userSchema);

module.exports = model;
