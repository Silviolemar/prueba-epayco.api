const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const paymentControllers = require('../../controllers/v1/payments-controllers');

const routes = express.Router();

routes.post('/savePayment', isAuth, paymentControllers.savePayment);
routes.post('/sendMail', isAuth, paymentControllers.sendMail);

module.exports = routes;
