const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const walletControllers = require('../../controllers/v1/wallets-controllers');

const routes = express.Router();

routes.post('/charge', isAuth, walletControllers.charge);

module.exports = routes;
