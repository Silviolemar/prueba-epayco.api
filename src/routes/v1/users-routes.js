const express = require('express');
const userControllers = require('../../controllers/v1/users-controllers');

const routes = express.Router();

routes.post('/create', userControllers.createUser);
routes.post('/login', userControllers.login);

module.exports = routes;
