const usersRoutes = require('./users-routes');
const walletsRoutes = require('./wallet-routes');
const paymentsRoutes = require('./payment-routes');
const epaycoRoutes = require('./epayco-routes');

module.exports = (app) => {
  app.use('/api/v1/users', usersRoutes);
  app.use('/api/v1/wallets', walletsRoutes);
  app.use('/api/v1/payments', paymentsRoutes);
  app.use('/api/v1/epayco', epaycoRoutes);
};
