const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const epaycoControllers = require('../../controllers/v1/epayco-controllers');

const routes = express.Router();

routes.post('/createToken', isAuth, epaycoControllers.createToken);
routes.post('/createCustomer', isAuth, epaycoControllers.createCustomer);
routes.get('/retrieve', isAuth, epaycoControllers.retrieve);

module.exports = routes;
