const { text } = require("express");

const info = (text) => console.log('INFO', text);
const error = (text) => console.log('Error', text);

module.exports.info = info;
module.exports.error = error;
