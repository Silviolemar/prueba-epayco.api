const epayco = require('epayco-sdk-node')({
  apiKey: '75faf8321bb999f89f0998ce8a0c6f10',
  privateKey: 'b6005acaf77a99c1a38d887b8bb91ecc',
  lang: 'ES',
  test: true,
});

let tokeId = '';
let idCustomer = '';

const createToken = async (_, res) => {
  const creditInfo = {
    'card[number]': '4575623182290326',
    'card[exp_year]': '2025',
    'card[exp_month]': '12',
    'card[cvc]': '123',
  };
  epayco.token.create(creditInfo)
    .then((token) => {
      console.log(token.id);
      tokeId = token.id;
      res.status(200).send({
        token,
      });
    })
    .catch((err) => {
      console.log(`err: ${err}`);
      res.send({
        err,
      });
    });
};

const createCustomer = async (_, res) => {
  const customerInfo = {
    token_card: tokeId,
    name: 'Joe',
    last_name: 'Doe',
    email: 'joe@payco.co',
    default: true,
    city: 'Bogota',
    address: 'Cr 4 # 55 36',
    phone: '3005234321',
    cell_phone: '3010000001',
  };
  epayco.customers.create(customerInfo)
    .then((customer) => {
      console.log(customer);
      idCustomer = customer.data.customerId;
      console.log(idCustomer);
      res.status(200).send({
        customer,
      });
    })
    .catch((err) => {
      console.log(`err: ${err}`);
      res.send({
        err,
      });
    });
};

const retrieve = async (req, res) => {
  console.log(idCustomer);
  epayco.customers.get(idCustomer)
    .then((customer) => {
      console.log(customer);
      res.status(200).send({
        customer,
      });
    })
    .catch((err) => {
      console.log(`err: ${err}`);
      res.send({
        err,
      });
    });
};

module.exports = {
  createToken,
  createCustomer,
  retrieve,
};
