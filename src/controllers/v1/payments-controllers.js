/* eslint-disable no-mixed-operators */
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const User = require('../../mongo/models/user');
const Wallet = require('../../mongo/models/wallet');
const Payment = require('../../mongo/models/payment');

const savePayment = async (req, res) => {
  try {
    const {
      orderNumber, amount, walletId,
    } = req.body;

    if (orderNumber !== '' && amount !== '') {
      const payment = new Payment();
      payment.orderNumber = orderNumber;
      payment.amount = amount;
      payment.wallet = walletId;

      await payment.save();

      const wallet = await Wallet.findById(walletId);

      const subtractionValue = parseFloat(wallet.value) - parseFloat(amount);

      await Wallet.findByIdAndUpdate(walletId, {
        value: subtractionValue,
      });

      res.status(200).send({
        status: 'OK',
        message: 'Payment saved',
        data: {
          subtractionValue,
        },
      });
    } else {
      res.status(400).send({ status: 'INCORRECT_DATA', message: 'Incorrect data' });
    }
  } catch (error) {
    if (error.code && error.code === 11000) {
      res
        .status(400)
        .send({ status: 'DUPLICATED_VALUES', message: error.keyValue });
      return;
    }
    res.status(500).send({ status: 'ERROR', message: error.message });
  }
};

const sendMail = (req, res) => {
  const {
    orderNumber, amount, userEmail,
  } = req.body;

  if (orderNumber !== '' && amount !== '') {
    const randToken = crypto.randomBytes(3).toString('hex');
    const tansporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
      },
    });

    const mailOptios = {
      from: process.env.MAIL_SENDER,
      to: userEmail, // list of receivers
      subject: 'Correo de verificación Epayco ✔️', // Subject line
      text: `Codigo de verificación: ${randToken}\nNumero de orden: ${orderNumber}\nMonto a pagar: $${amount}`, // plain text body
      html: `<b>Billetera Epayco<p>Codigo de verificación: ${randToken}<p>Numero de orden: ${orderNumber}<p>Monto a pagar: $${amount}</b>`, // html body
    };

    tansporter.sendMail(mailOptios, (error, info) => {
      if (error) {
        res.status(500).send(error.message);
      } else {
        console.log('Mail successfully sent');
        res.status(200).send({
          status: 'OK',
          message: 'Mail successfully sent',
          data: {
            randToken,
            orderNumber,
            amount,
          },
        });
      }
    });
  } else {
    res.status(422).send({ status: 'ERROR', message: 'Incorrect data' });
  }
};

module.exports = {
  savePayment,
  sendMail,
};
