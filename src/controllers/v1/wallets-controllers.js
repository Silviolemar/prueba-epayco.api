const User = require('../../mongo/models/user');
const Wallet = require('../../mongo/models/wallet');

const charge = async (req, res) => {
  try {
    const {
      document, phone, value,
    } = req.body;

    const user = await User.findById(req.sessionData.userId);

    const wallet = await Wallet.findById(user.wallet);

    const walletValue = wallet.value;

    const sumValue = parseFloat(walletValue) + parseFloat(value);

    if (user.document === document && user.phone === phone && value !== '') {
      await Wallet.findByIdAndUpdate(user.wallet, {
        value: sumValue,
      });

      res.send({
        status: 'OK',
        message: 'Recharge successful',
        data: {
          sumValue,
        },
      });
    } else {
      res.status(422).send({ status: 'ERROR', message: 'Incorrect data' });
    }
  } catch (e) {
    res.status(500).send({ status: 'ERROR', message: e.message });
  }
};

module.exports = {
  charge,
};
