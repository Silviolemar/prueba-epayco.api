const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const validator = require('email-validator');
const User = require('../../mongo/models/user');
const Wallet = require('../../mongo/models/wallet');

const expiresIn = 60 * 10;

const login = async (req, res) => {
  try {
    const {
      email, password,
    } = req.body;
    const user = await User.findOne({ email });
    if (user === null) {
      res.status(401).send({ status: 'USER_NOT_FOUND', message: '' });
      return;
    }
    const wallet = await Wallet.findById(user.wallet);
    const userName = user.name;
    if (user) {
      const isOk = await bcrypt.compare(password, user.password);
      if (isOk) {
        const token = jwt.sign(
          { userId: user.id, role: user.role },
          process.env.JWT_SECRET,
          { expiresIn },
        );
        res.send({
          status: 'OK',
          data: {
            token,
            email,
            userName,
            wallet,
            expiresIn,
          },
        });
      } else {
        res.status(403).send({ status: 'INVALID_PASSWORD', message: '' });
      }
    } else {
      res.status(401).send({ status: 'USER_NOT_FOUND', message: '' });
    }
  } catch (e) {
    res.status(500).send({ status: 'ERROR', message: e.message });
  }
};

const createUser = async (req, res) => {
  let walletId = '';
  try {
    const {
      email, password, document, name, phone,
    } = req.body;

    const hash = await bcrypt.hash(password, 15);

    const findUser = await User.findOne({ email });

    if (findUser === null) {
      const user = new User();
      user.email = email;
      user.password = hash;
      user.document = document;
      user.name = name;
      user.phone = phone;

      const wallet = new Wallet();
      wallet.name = `Billetera ${name}`;
      wallet.value = 0.0;

      await wallet.save();

      walletId = wallet.id;
      user.wallet = wallet.id;

      await user.save();

      res.status(200).send({ status: 'OK', message: 'user created' });
    } else {
      res
        .status(400)
        .send({ status: 'DUPLICATED_VALUES', message: 'Duplicated user' });
    }
  } catch (error) {
    if (error.code && error.code === 11000) {
      if (!validator.validate(req.body.email)) {
        res
          .status(400)
          .send({ status: 'INCORRECT_EMAIL', message: 'Incorrect email' });
        return;
      }
      res
        .status(400)
        .send({ status: 'DUPLICATED_VALUES', message: 'Duplicated user' });
      return;
    }
    res.status(500).send({ status: 'ERROR', message: error.message });
  }
};

module.exports = {
  createUser,
  login,
};
